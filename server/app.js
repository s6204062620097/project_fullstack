require('dotenv').config();
require('./config/database').connect();

const express = require("express")
const User = require("./models/users")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")

const app = express();

app.use(express.json());

//Login
app.post("/login", (req, res) => {
    // login logic
})

//Register
app.post("/register", async (req, res) => {
    // register logic
    try{
        //Get user input
        const { UserID, FirstName, LastName, Address, Email, Password, TelNo, Type } = req.body;

        //Validate
        if(!( UserID, FirstName && LastName && Address && Email && Password && TelNo && Type )) {
            res.status(400).send("All input is required");
        }

        //User already exists
        const oldUser = await User.findOne({ Email });
        console.log(oldUser)
        if(oldUser) {
            return res.status(409).send("User already exist.")
        }

        //Encrypt password
        encryptedPassword = await bcrypt.hash( Password, 10 );

        //Creat User
        const user = await User.create({
            UserID,
            FirstName,
            LastName,
            Address,
            Email: Email.toLowerCase(),
            Password: encryptedPassword,
            TelNo,
            Type
        })

        //Creat Token
        const token = jwt.sign(
            { user_id: user._id, Email },
            process.env.TOKEN_KEY,
            {
                expiresIn: "1h"
            }
        )

        //Save Token
        user.Token = token;

        //return new user
        res.status(201).json(user);

    }catch (err){
        console.log(err);
    }
})

module.exports = app;