const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    UserID: { type: 'String', primaryKey: true },
    FirstName: { type: 'String', default: null },
    LastName: { type: 'String', default: null },
    Address: { type: 'String', default: null },
    Email: { type: 'String', unique: true},
    Password: { type: 'String' },
    TelNo: { type: 'String' },
    Type: { type: 'String' },
    Token: { type: 'String' },
})

module.exports = mongoose.model('user', userSchema);